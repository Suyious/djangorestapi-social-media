# Django Test Driven Social Media REST API

### Basic Project Setup
```bash
mkdir DjangoRESTAPI && cd $_
python3 -m venv .venv
. .venv/bin/activate
pip install django djangorestframework
pip freeze > requirements.txt
git init
curl https://raw.githubusercontent.com/github/gitignore/main/Python.gitignore > .gitignore
django-admin startproject config .
```

This should set up a boilerplate project with project configs and initialized git repository.
The Project Structure should resemble the following.
```bash
$ tree -a --gitignore -I ".git/"
.
├── .gitignore
├── config
│   ├── __init__.py
│   ├── asgi.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
├── manage.py
└── requirements.txt
```
```bash
$ cat requirements.txt 
asgiref==3.6.0
backports.zoneinfo==0.2.1
Django==4.1.6
djangorestframework==3.14.0
pytz==2022.7.1
sqlparse==0.4.3
```

### Initial Commit: Getting Started

```bash
mkdir -p src/user
touch src/__init__.py
python3 manage.py startapp user src/user
```

The Project Structure should resemble the following:
```bash
$ tree src/
src/
├── __init__.py
└── user
    ├── __init__.py
    ├── admin.py
    ├── apps.py
    ├── migrations
    │   └── __init__.py
    ├── models.py
    ├── tests.py
    └── views.py
```

Now, to add the user app to the project, edit `src/user/apps.py`
```py
from django.apps import AppConfig

class UserConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'src.user'
```
and then edit `config/settings.py`
```py
# ...

INSTALLED_APPS = [
    'django.contrib.admin',
	# ...
    'src.user'
]
# ...
```

Now we can run migrations and start the development server.
```bash
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py runserver
```
This should start the development server on locahost. Now we can open the project in the code editor. Also we can commit our changes to the git repository.
